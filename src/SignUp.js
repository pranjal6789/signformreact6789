import React, { Component } from "react";
import "./SignUp.css";
class Signup extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      email: "",
      password: "",
      AgainPassword: "",
      nameerror: "",
      emailerror: "",
      passworderror: "",
      AgainPasswordError: "",
      data: "",
    };
  }
  validate = () => {
    const patternForEmail = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;
    const name = this.state.name.trim();
    if (name === null || name.length < 2 || name.includes(".")) {
      console.log(name.length);
      this.setState({
        nameerror: "Please Type name",
      });
      return false;
    }
    if (!this.state.email.match(patternForEmail)) {
      this.setState({
        emailerror: "Invalid Email",
      });
      return false;
    }

    if (this.state.password.length < 6) {
      this.setState({
        passworderror: "Password is too short",
      });
      return false;
    }
    if (this.state.AgainPassword !== this.state.password) {
      this.setState({
        AgainPasswordError: "Password not matches",
      });
      return false;
    }
    return true;
  };
  handleSubmit = (event) => {
    event.preventDefault();
    let isValid = this.validate();

    if (isValid) {
      console.log(this.state);
      this.setState({
        name: "",
        email: "",
        password: "",
        AgainPassword: "",
        nameerror: "",
        emailerror: "",
        passworderror: "",
        AgainPasswordError: "",
        data: "Successfully signup",
      });
    }
  };
  handleName = (event) => {
    this.setState({
      name: event.target.value,
    });
  };
  handleEmail = (event) => {
    this.setState({
      email: event.target.value,
    });
  };
  handlePassword = (event) => {
    this.setState({
      password: event.target.value,
    });
  };
  handleAgainPassword = (event) => {
    this.setState({
      AgainPassword: event.target.value,
    });
  };
  render() {
    return (
      <div className="form-container">
        <form className="formData">
          <input
            type="text"
            placeholder="Name"
            className="input-field"
            value={this.state.name}
            onChange={this.handleName}
          />
          <div className="error">{this.state.nameerror}</div>
          <input
            type="text"
            placeholder="Email"
            className="input-field"
            value={this.state.email}
            onChange={this.handleEmail}
          />
          <div className="error">{this.state.emailerror}</div>
          <input
            type="password"
            placeholder="Password"
            className="input-field"
            value={this.state.password}
            onChange={this.handlePassword}
          />
          <div className="error">{this.state.passworderror}</div>
          <input
            type="password"
            placeholder="Again Enter Password"
            className="input-field"
            value={this.state.AgainPassword}
            onChange={this.handleAgainPassword}
          />
          <div className="error">{this.state.AgainPasswordError}</div>
          <button onClick={this.handleSubmit} className="button" type="submit">
            Sign Up
          </button>
        </form>
        <h1 className="textsizing">{this.state.data}</h1>
      </div>
    );
  }
}

export default Signup;
